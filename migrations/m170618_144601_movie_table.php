<?php

use yii\db\Migration;

class m170618_144601_movie_table extends Migration
{
    public function up()
    {
	$this->createTable('movie', [
            'id' => $this->primaryKey(),
			'movieName' => $this->String()->notNull(),
			'genre' => $this->String()->notNull(),
			'min_age' => $this->integer()->notNull(),
			'grade' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('movie');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
