<?php

use yii\db\Migration;

class m170618_141534_user_table extends Migration
{
    public function up()
    {
	 $this->createTable('user', [
            'id' => $this->primaryKey(),
			'username' => $this->String()->notNull(),
			'password' => $this->String()->notNull(),
			'auth_Key' => $this->String()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
