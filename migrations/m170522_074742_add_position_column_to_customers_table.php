<?php

use yii\db\Migration;

/**
 * Handles adding position to table `customers`.
 */
class m170522_074742_add_position_column_to_customers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		 $this->addColumn('customers', 'description', $this->string());
		  
		
    }

    public function down()
    {
		
		  $this->dropColumn('customers', 'description');
    }
}
