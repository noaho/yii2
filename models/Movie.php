<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movie".
 *
 * @property integer $id
 * @property string $movieName
 * @property string $genre
 * @property integer $min_age
 * @property integer $grade
 */
class Movie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['movieName', 'genre', 'min_age', 'grade'], 'required'],
            [['min_age', 'grade'], 'integer'],
            [['movieName', 'genre'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'movieName' => 'Movie Name',
            'genre' => 'Genre',
            'min_age' => 'Min Age',
            'grade' => 'Grade',
        ];
    }
}
